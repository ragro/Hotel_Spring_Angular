package com.example.intelli.hotel.IntelliHotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntelliHotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntelliHotelApplication.class, args);
	}
}
