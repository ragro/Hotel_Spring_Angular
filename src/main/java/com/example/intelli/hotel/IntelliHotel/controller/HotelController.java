package com.example.intelli.hotel.IntelliHotel.controller;

import com.example.intelli.hotel.IntelliHotel.model.Hotel;
import com.example.intelli.hotel.IntelliHotel.model.Review;
import com.example.intelli.hotel.IntelliHotel.services.HotelService;
import org.apache.commons.collections4.MapUtils;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/")
public class HotelController {

    @Autowired
    HotelService hotelService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Hotel> seedDB(){
        return hotelService.getHotels();
    }


    @RequestMapping(value = "/addHotel", method = RequestMethod.POST)
    public void addHotel(@RequestBody Hotel hotel){
//          System.out.println(hotel);
            System.out.println(hotelService.addNewHotel(hotel));
    }

    @RequestMapping(value = "/getHotel", method = RequestMethod.GET)
    public List<Hotel> getHotels(){
        return hotelService.getHotels();
    }

    @RequestMapping(value = "/deleteHotel", method = RequestMethod.POST)
    public void deleteHotel(@RequestBody String hotel_id){
        hotelService.deleteHotel(Long.parseLong(hotel_id));
    }

    @RequestMapping(value = "/addReview", method = RequestMethod.POST)
    public  void addReview(@RequestBody Map data  ){ //  continue from here

        Map<String, Object> map = (Map<String, Object>)data.get("review");

//        String comment = (String)map.get("comment");
//        Integer rating = Integer.parseInt((String)map.get("rating"));

        String comment = MapUtils.getString(map , "comment");
        Integer rating = MapUtils.getInteger(map, "rating");

        Long id = MapUtils.getLong(data, "id");

        Review  newReview = new Review();
        newReview.setComment(comment);
        newReview.setRating(rating);

        this.hotelService.addReview(id, newReview);
    }

    @RequestMapping(value = "/deleteReview", method = RequestMethod.POST)
    public void deleteReview(@RequestBody Map ids){
        Long reviewId = MapUtils.getLong(ids,"review_id");
        Long hotelId = MapUtils.getLong(ids, "hotel_id");

        this.hotelService.deleteReview(hotelId, reviewId);
    }
}














