package com.example.intelli.hotel.IntelliHotel.repositories;

import com.example.intelli.hotel.IntelliHotel.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepo extends JpaRepository<Hotel, Long> {
}
